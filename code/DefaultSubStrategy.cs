﻿namespace ProviderSys;

public class DefaultSubStrategy : SubStrategy
{
  public override decimal GetCost() => _cost;
}