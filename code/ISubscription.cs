﻿namespace ProviderSys;

public interface ISubscription
{
  SubStrategy Strategy { get; set; }
  string Title { get; set; }
  string Description { get; set; }
  decimal Cost { get; set; }
  DateTime EndDate { get; set; }
  void Extend(int monthCount);
  string GetInfo();
}