﻿namespace ProviderSys;

public class InternetSubController
{
  public InternetSubscription? GetSubscription(SubParameters parameters) =>
      (InternetSubscription)new ProviderFacade<InternetSubFactory>().CreateSubscription(parameters);
}