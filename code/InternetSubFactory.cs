﻿using ProviderSys;

namespace ProviderSys;

public class InternetSubFactory : SubFactory
{
  public override ISubscription? MakeSubscription(SubParameters parameters)
  {
    switch (parameters)
    {
      case SubParameters.Default:
        return new InternetSubscription(new DefaultSubStrategy());
      case SubParameters.WithDiscount:
        return new InternetSubscription(new WithDiscountSubStrategy());
      default:
        return null;
    }
  }
}