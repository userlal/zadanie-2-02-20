﻿using ProviderSys;

namespace ProviderSys;

public class InternetSubscription : ISubscription
{
  public SubStrategy Strategy { get; set; }
  public string Title { get; set; }
  public string Description { get; set; }
  public decimal Cost { get; set; }
  public DateTime EndDate { get; set; }

  public InternetSubscription(SubStrategy strategy)
  {
    Strategy = strategy;
    Title = "INTERNET";
    Description = "Пакет Домашнего Интернета";
    Cost = Strategy.GetCost();
    EndDate = DateTime.Now.AddMonths(1);
  }
  public void Extend(int monthCount) => EndDate = EndDate.AddMonths(monthCount);
  public string GetInfo() => $"{Title} - {Description} - {Cost} - {EndDate}";
}