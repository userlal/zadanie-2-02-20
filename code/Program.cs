﻿using System;
using System.Runtime.InteropServices;

namespace ProviderSys;

class Program
{
  public static void Main()
  {
    var tvController = new TvSubController();
    var defaultTvSubscription = tvController.GetSubscription(SubParameters.Default);
    var withDiscountTvSubscription = tvController.GetSubscription(SubParameters.WithDiscount);

    var intController = new InternetSubController();
    var defaultIntSub = intController.GetSubscription(SubParameters.Default);
    var withDiscountIntSub = intController.GetSubscription(SubParameters.WithDiscount);

    Console.WriteLine(defaultTvSubscription?.GetInfo());
    Console.WriteLine(withDiscountTvSubscription?.GetInfo());
    Console.WriteLine(defaultIntSub?.GetInfo());
    Console.WriteLine(withDiscountIntSub?.GetInfo());

  }
}