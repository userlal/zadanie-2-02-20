﻿using ProviderSys;

namespace ProviderSys;

public class ProviderFacade<T> where T : SubFactory, new()
{
  private T _factory;

  public ProviderFacade()
  {
    _factory = new();
  }

  public ISubscription? CreateSubscription(SubParameters parameters) =>
      _factory.MakeSubscription(parameters);

  public void ExpandSubscription(ISubscription subscription, int monthCount) => subscription.Extend(monthCount);
}