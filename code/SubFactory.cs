﻿using ProviderSys;

namespace ProviderSys;

public abstract class SubFactory
{
  public SubFactory()
  {
  }

  public abstract ISubscription? MakeSubscription(SubParameters subscriptionParameters);
}