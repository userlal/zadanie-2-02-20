﻿namespace ProviderSys;

public abstract class SubStrategy
{
  protected decimal _cost = 1000;

  public SubStrategy()
  {
  }

  public abstract decimal GetCost();
}