﻿using ProviderSys;

namespace ProviderSys;

public class TvSubController
{
  public TvSubscription? GetSubscription(SubParameters parameters) =>
      (TvSubscription)new ProviderFacade<TvSubFactory>().CreateSubscription(parameters);
}