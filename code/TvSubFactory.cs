﻿using ProviderSys;

namespace ProviderSys;

public class TvSubFactory : SubFactory
{
  public override ISubscription? MakeSubscription(SubParameters subscriptionParameters)
  {
    switch (subscriptionParameters)
    {
      case SubParameters.Default:
        return new TvSubscription(new DefaultSubStrategy());
      case SubParameters.WithDiscount:
        return new TvSubscription(new WithDiscountSubStrategy());
      default:
        return null;
    }
  }
}