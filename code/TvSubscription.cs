namespace ProviderSys;

public class TvSubscription : ISubscription
{
    public SubStrategy Strategy { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public decimal Cost { get; set; }
    public DateTime EndDate { get; set; }

    public TvSubscription(SubStrategy strategy)
    {
        Strategy = strategy;
        Title = "TV";
        Description = "300 TV �������";
        Cost = Strategy.GetCost();
        EndDate = DateTime.Now.AddMonths(1);
    }
    public void Extend(int monthCount) => EndDate = EndDate.AddMonths(monthCount);
    public string GetInfo() => $"{Title} - {Description} - {Cost} - {EndDate}";
}