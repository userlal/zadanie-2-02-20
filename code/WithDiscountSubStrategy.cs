﻿using ProviderSys;
using System.Runtime.CompilerServices;

namespace ProviderSys;

public class WithDiscountSubStrategy : SubStrategy
{
  private int _discountPercent = 15;
  public WithDiscountSubStrategy()
  {
  }

  public override decimal GetCost() => _cost - (_cost / 100 * _discountPercent);
}